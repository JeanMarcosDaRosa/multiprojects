<?php
	$callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
	$ultima = isset($_GET['uldata']) ? preg_replace('/[^0-9]/', '', $_GET['uldata']) : false;
	$aplicacao = isset($_GET['aplicacao']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['aplicacao']) : false;
	header('Content-Type: ' . ($callback ? 'application/javascript' : 'application/json') . ';charset=UTF-8');

	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => 'http://'.$_SERVER['HTTP_HOST'].':9000/list?uldata='.$ultima.'&aplicacao='.$aplicacao
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	if($resp!=false){
		echo ($callback ? $callback . '(' : '') . json_encode($resp) . ($callback ? ')' : '');
	}
	
	// Close request to clear up some resources
	curl_close($curl);

?>