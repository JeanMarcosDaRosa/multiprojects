<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/styles.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/ui.spinner.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="js/plugins/tables/jquery.sortable.js"></script>
        <script type="text/javascript" src="js/plugins/tables/jquery.resizable.js"></script>
        <script type="text/javascript" src="js/plugins/forms/autogrowtextarea.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.uniform.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.inputlimiter.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.autotab.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.chosen.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.dualListBox.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.cleditor.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.ibutton.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="js/plugins/forms/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="js/plugins/wizards/jquery.form.wizard.js"></script>
        <script type="text/javascript" src="js/plugins/wizards/jquery.validate.js"></script>
        <script type="text/javascript" src="js/plugins/wizards/jquery.form.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.collapsible.min.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.breadcrumbs.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.tipsy.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.progress.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.timeentry.min.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.colorpicker.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.jgrowl.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.fancybox.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.fileTree.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.sourcerer.js"></script>
        <script type="text/javascript" src="js/plugins/others/jquery.fullcalendar.js"></script>
        <script type="text/javascript" src="js/plugins/others/jquery.elfinder.js"></script>
        <script type="text/javascript" src="js/plugins/ui/jquery.easytabs.min.js"></script>
        <script type="text/javascript" src="js/files/bootstrap.js"></script>
        <script type="text/javascript" src="js/files/functions.js"></script>
        <script type="text/javascript">
            stop = false;
            ultimadata = undefined;
            appclient = undefined;
        </script>
    </head>
    <body>
        <!-- Content begins -->    
        <div id="content" style="margin-left: 0px!important; padding-top: 0px!important; ">
            <div class="contentTop">
                <div class="widget grid12">
                    <ul class="tabs">
                        <li><a href="#tabb1">Todos</a></li>
                        <li><a href="#tabb2">Erros</a></li>
                    </ul>
                    <div class="tab_container">
                        <div id="tabb1" class="tab_content">
                            <div id="tabcontent1" style="padding: 10px 10px 10px 10px; background-image: url('images/backgrounds/sidebar.jpg'); max-height: 400px!important; overflow-y: scroll;">
                                <font color="white">
                                    <div id="conteudoLogTodos">
                                    </div>
                                </font>
                            </div>
                        </div>
                        <div id="tabb2" class="tab_content">
                            <div id="tabcontent2" style="padding: 10px 10px 10px 10px; background-image: url('images/backgrounds/sidebar.jpg'); max-height: 400px!important; overflow-y: scroll;">
                                <font color="white">
                                    <div id="conteudoLogErros">
                                    </div>
                                </font>
                            </div>
                        </div>
                    </div>  
                    <div class="clear"></div>        
                </div>
                <ul class="middleNavR" style="margin: 5px 0 0 0 !important;">
                    <li><a href="#" class="tipN" onclick="stop=false;"><img src="images/icons/middlenav/play.png" alt="" /></a></li>
                    <li><a href="#" class="tipN" onclick="stop=true;"><img src="images/icons/middlenav/stop.png" alt="" /></a></li>
                </ul>
            </div>
        </div>  
    </body>
    <script type="text/javascript">
        $(function () {
            setInterval(function(){
                if(!stop){
                    $.ajax({
                        type: 'GET',
                        url: 'getlogs.php/list?callback=?',
                        async: false,
                        jsonpCallback: 'jsonCallbacklist',
                        contentType: "application/json",
                        dataType: 'jsonp',
                        data: {uldata: ultimadata, aplicacao: appclient}
                    }).done(function(json){
                        var msgAll = "", msgErr = "";
                        if(!(json instanceof Object)){
                            json = JSON.parse(json);
                        }
                        if(json.logs!=undefined){
                            for (var i = 0; i < json.logs.length; i++) {
                                //if((json.logs[i].mensagem).indexOf(" ERROR ")>0 || (json.logs[i].mensagem).indexOf("Exception")>0){
                                //    msgErr = msgErr+json.logs[i].mensagem+'<br/>';
                                //}else{
                                    msgAll = msgAll+json.logs[i].mensagem+'<br/>';
                                //}
                                ultimadata = json.logs[i].timestamp;
                            };
                        }else{
                            if(json.apps!=undefined && json.apps!="null"){
                                for (var i = 0; i < json.apps.length; i++) {
                                    appclient = json.apps[i];
                                    console.log(json.apps[i]);
                                }
                            }
                        }
                        $('#conteudoLogErros').append(msgErr);
                        $('#conteudoLogTodos').append(msgAll);
                        var $t = $('#tabcontent1');
                        $t.animate({"scrollTop": $t[0].scrollHeight}, "fast");
                    }).fail(function(e){
                        console.log("Falha no GET");
                    });
                }
            }, 2000);
        });
    </script>
</html>