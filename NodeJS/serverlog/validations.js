/**
  Modulo de controle de validações
*/
console.info("--> Carregou módulo validations");

var collectionUser;

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'email@gmail.com',
        pass: 'senha'
    }
});

exports.getListLogs = function (req, res){
  //logsList = [];
  if(req.query.uldata === undefined || req.query.uldata === 'null' || 
    req.query.uldata === 'undefined' || req.query.uldata === ''){
      var copy = logsList[req.query.aplicacao];
      var list = [];
      if(copy!=undefined){
        res.jsonp({'logs':copy});
      }else{
        Object.keys(logsList).forEach(function(key) {
          list.push(key);
        });
        res.jsonp({'apps':list});
      }
  }else{
    var time = parseInt(req.query.uldata, 10);
    var copy = logsList[req.query.aplicacao];
    var list = [];
    if(copy!=undefined){
      for (var i = copy.length - 1; i >= 0; i--) {
        if(copy[i].timestamp>time){
          list.push(copy[i]);
        }else{
          break;
        }
      };
      res.jsonp({'logs':list});
    }else{
      Object.keys(logsList).forEach(function(key) {
        list.push(key);
      });
      res.jsonp({'apps':list});
    }
  }
}

exports.insertLog = function (appclient, mensagem, callback){
  var appLog = logsList[appclient] || [];
  if(appLog.length >= qtdLogs){
    //remove o primeiro elemento
    appLog.splice(0, 1);
  }
  appLog.push({'timestamp': (new Date().getTime()), 'mensagem': mensagem});
  logsList[appclient] = appLog;
  callback();
}

var sendMail = function(email, subjectMail, htmlMail){
  transporter.sendMail({
      from: 'Nome <email@gmail.com>',
      to: email,
      subject: subjectMail,
      html: htmlMail
  }, function(error, info){
      if(error){
          console.error(error);
      }
  });
}

if (enablePagesServer){
  http.createServer(function(request, response) {
    var uri = url.parse(request.url).pathname, filename = path.join(process.cwd(), uri);
    if(uri==='/'){
      uri=uri+'html/index.html';
      response.writeHead(301, {Location: uri} );
      response.end();
      return;
    }
    fs.exists(filename, function(exists) {
      if(!exists) {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not Found\n");
        response.end();
        return;
      }
      if (fs.statSync(filename).isDirectory()) filename += '/index.html';
      fs.readFile(filename, "binary", function(err, file) {
        if(err) {
          response.writeHead(500, {"Content-Type": "text/plain"});
          response.write(err + "\n");
          response.end();
          return;
        }
        response.writeHead(200);
        response.write(file, "binary");
        response.end();
      });
    });
  }).listen(parseInt(portHttp, 10));
}

exports.validaEmail = function (email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

var makeMsgAuthHtm = function(email, log, host){
  mail=(new Buffer(email).toString('base64')).replace('=','');
  return '<!DOCTYPE html>'+
        '<html>'+
            '<head>'+
                '<link href="http://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css"/>'+
            '</head>'+
            '<body>'+
                'Encontramos um erro no log da aplicacao [app-nome] no cliente [cliente-nome], favor verificar:<br/>'+
                '<a href="http://'+host+'/list" target="_blank" style="position: relative;'+
                'padding: 0px 40px;margin: 0px 10px 10px 0px;float: left;border-radius: 10px;font-family: Pacifico, cursive;font-size: 25px;'+
                'color: #FFF;text-decoration: none; transition: all 0.1s; -webkit-transition: all 0.1s; background-color: #3498DB;border-bottom: 5px solid #2980B9;'+
                'text-shadow: 0px -2px #2980B9;">Ir para a pagina!</a><br/><br/><br/><br/>'+
                'Log: <br/>'+log+
            '</body>'+
        '</html>';
}

exports.getURLParameter = function (body, name) {
  return fix(decodeURI( (RegExp(name + '=' + '(.+?)(&|$)').exec(body)||[,null])[1] ));
}

exports.decode = function (element) {
  return decodeURIComponent(element.replace(/\+/g,  " "));
}

exports.encode = function (element) {
  return encodeURIComponent(element).replace(/'/g,"%27").replace(/"/g,"%22"); 
}

var fix = function(variable){
  if(variable.charAt(0)==='&'){
    return "";
  }else{
    return variable;
  }
}

exports.convertBase64ToString = function(text){
  if(text.trim()==="") return text;
  return ic.convert(new Buffer(text, 'base64')).toString("utf8");
}

exports.validaInputUser = function(text){
  return text.replace("<", "&lt;").replace(">", "&gt;");
}

var getIpClient = function(req){
  return req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
}
