/**
  Modulo principal de controle de fluxo
*/

//bloco de requires
os          = require('os');
colors      = require('colors');
express     = require('express');
app         = express();
format      = require('util').format;
fs          = require("fs");
url         = require("url");
domain      = require('domain').create();
nodemailer  = require('nodemailer');
http        = require("http");
path        = require("path");
formidable  = require('formidable');
util        = require('util');
fse         = require('fs-extra');
md5         = require('MD5');

//variaveis de uso geral
sistema     = os.type().toLowerCase();
portRest    = 9000; //sem o prefixo 'var', a variavel torna-se global entre os modulos
portHttp    = 9001;
qtdLogs     = 1024;
enablePagesServer = false;
logsSystem  = [];
logsList    = {};
verbose     = true;
debug       = true;
startTime   = new Date();

// set theme
colors.setTheme({
  verbose:  'cyan',
  info:     'green',
  warn:     'yellow',
  debug:    'blue',
  help:     'blue',
  error:    'red'
});

//Definição dos consoles e cores
console.info    = function(message, module, functionBy, line){message=message+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":""); console.log(colors.info(message)); }
console.error   = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [error]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; console.log(colors.error(message)); }
console.verbose = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [verbose]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; console.log(colors.verbose(message)); } 
console.warn    = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [warn]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; console.log(colors.warn(message)); } 
console.debug   = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [debug]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; console.log(colors.debug(message)); } 
console.help    = function(message){ console.log(colors.debug(message)); }

//Rota de erro
domain.on('error', function(err){
    console.error(err.stack);
});

var showHelp = function(){
	console.help('Use:');
	console.help('\t--httpserver | -s para ativar o servidor de paginas html');
	console.help('\t--hs-port para definir a porta do servidor de paginas html');
	console.help('\t--rest-port para definir a porta do servico REST');
	console.help('\t--qtdlogs | -qL [valor] para definir o tamanho do historico de logs');
	console.help('\t--help | -h para exibir este menu');
}

domain.run(function() {
    console.info('\n    """""""--....... TN3 Server Logs .......--"""""""');
    console.info('/-----------------------------------------------------\\');
    console.info('Start : '+new Date());
    console.info('OS: '+sistema);
    var isHelp = false, nextIsLog = false, nextIsHsPort=false, nextIsRPort=false, changeMD5 = false, params = false;
    process.argv.forEach(function (val, index, array) {
      if(index>=2){
        params = true;
        console.info('Parameter: \t' + val);
        if(nextIsLog){
            qtdLogs = parseInt(val,10);
            nextIsLog=false;
        }if(nextIsHsPort){
            portHttp = parseInt(val,10);
            nextIsHsPort=false;
        }if(nextIsRPort){
            portRest = parseInt(val,10);
            nextIsRPort=false;
        }else{
            if(val.toLowerCase()==="--qtdlogs"          || val.toLowerCase()==="-qL")    nextIsLog = true;
            if(val.toLowerCase()==="--httpserver"       || val.toLowerCase()==="-s" )    enablePagesServer = true;
            if(val.toLowerCase()==="--hs-port"       								)    nextIsHsPort = true;
            if(val.toLowerCase()==="--rest-port"       								)    nextIsRPort = true;
            if(val.toLowerCase()==="--htmlserver"       || val.toLowerCase()==="-s" )    enablePagesServer = true;
            if(val.toLowerCase()==="--help"             || val.toLowerCase()==="-h" ){
            	console.info('\\-----------------------------------------------------/');
            	showHelp();
            	isHelp = true;
            }
        }
      }
    });
    
    if(isHelp) process.exit(code=0);

    if(!params) showHelp();

    //parametros do servidor
    app.set("jsonp callback", true);
    app.enable("jsonp callback");

    app.use(function (req, res, next) {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);
        // Pass to next layer of middleware
        next();
    });

    val = require('./validations');
    app.get('/list', val.getListLogs);
    app.post('/log', function(req, res){
        var body = '';
        req.on('data', function (data) {
            body += data;
        });
        req.on('end', function () {
            var log = val.decode(decodeURI(val.getURLParameter(body, 'logger')));
            var appclient = val.decode(decodeURI(val.getURLParameter(body, 'aplicacao')));
            val.insertLog(appclient, log, function(){
                //console.info(log);
                res.jsonp({'response' : 'OK'});
            });
        });
    });

    //coloca o servidor REST para rodar na porta pre-definida
    app.listen(portRest);

    //mostra o status do servidor nodejs
    console.info("Services-----------------------------------------------");
    if (enablePagesServer) console.info('HTTP port: \t'+portHttp);
    console.info('REST port: \t'+portRest);
    console.info('\\-----------------------------------------------------/');
});