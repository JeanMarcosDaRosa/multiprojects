/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tn3.httplogger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.spi.LoggingEvent;

/**
 *
 * @author jean.marcos
 */
public class HttpAppenderTn3 extends org.apache.log4j.AppenderSkeleton {

    private String url = null;
    private String cliente = null;
    private String aplicacao = null;
    private HttpURLConnection connector;
    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36";

    @Override
    protected void append(LoggingEvent event) {
        sendLog(event);
    }

    public void sendLog(LoggingEvent event) {
        try {
            CookieHandler.setDefault(new CookieManager());
            Map<String, String> mapaParametros = new HashMap<String, String>();
            mapaParametros.put("logger", layout.format(event));
            mapaParametros.put("cliente", cliente);
            mapaParametros.put("aplicacao", aplicacao);
            String postParams = getParamsString(mapaParametros);
            sendPost(postParams, false);
        } catch (UnsupportedEncodingException ex) {
        }
    }

    private void sendPost(String postParams, Boolean getResult) {
        try {
            if (url != null && !url.equals("")) {
                URL obj = new URL(url);
                connector = ((HttpURLConnection) obj.openConnection());

                connector.setUseCaches(false);
                connector.setRequestMethod("POST");
                connector.setRequestProperty("Host", url);
                connector.setRequestProperty("User-Agent", USER_AGENT);
                connector.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                connector.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                connector.setRequestProperty("Connection", "keep-alive");
                connector.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connector.setRequestProperty("Content-Length", Integer.toString(postParams.length()));
                connector.setConnectTimeout(1000);
                connector.setDoOutput(true);
                connector.setDoInput(true);

                DataOutputStream wr = new DataOutputStream(connector.getOutputStream());
                wr.writeBytes(postParams);
                wr.flush();
                wr.close();
                
                int responseCode = connector.getResponseCode();
                if (getResult) {
                    if (responseCode == 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(connector.getInputStream()));
                        String inputLine;
                        StringBuilder response = new StringBuilder();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        System.out.println(response.toString());
                    }
                }
            }
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
    }

    public String getParamsString(Map<String, String> parametros) throws UnsupportedEncodingException {
        List<String> paramList = new ArrayList<String>();
        for (Map.Entry<String, String> entrySet : parametros.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue();
            paramList.add(key + "=" + URLEncoder.encode(value, "UTF-8"));
        }
        StringBuilder result = new StringBuilder();
        for (String param : paramList) {
            if (result.length() == 0) {
                result.append(param);
            } else {
                result.append("&").append(param);
            }
        }
        return result.toString();
    }

    @Override
    public boolean requiresLayout() {
        return true;
    }

    @Override
    public void close() {
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the aplicacao
     */
    public String getAplicacao() {
        return aplicacao;
    }

    /**
     * @param aplicacao the aplicacao to set
     */
    public void setAplicacao(String aplicacao) {
        this.aplicacao = aplicacao;
    }

}
