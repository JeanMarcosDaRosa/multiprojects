/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaselenium;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author jean.marcos
 */
public class JavaSelenium {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "C:\\phantomjs-2.0.0-windows\\bin\\phantomjs.exe");
        WebDriver driver = new PhantomJSDriver(caps);
        
        System.out.println("Inicio: "+(new Date()).toString());
        for(int i=0;i<10;i++){
            driver.get("http://www.issnetonline.com.br/homologaabrasf/online/NotaDigital/NovoLayoutNovaNota.aspx?EF+4E+50+D1+44+B1+18+A8+DF+5C+59+97+6E+CD+6B+EA+28+C1+48+96+F3+B9+7A+84+A4+F9+24+D9+AD+FF+88+E3+E7+F3+93+61+44+4D+32+AF+F2+8B+63+43+B0+7E+C7+14+F6+37+4B+11+D2+23+EE+EA+D2+67+6E+15+94+3F+F7+46+A0+6A+84+A4+63+FE+C9+BF+9+AF+A2+83+D1+9A+6D+A9+87+D3+A9+5F+42+8D+A7+95+72+D2+63+9D+B7+53+49+FE+52+B0+86+5D+58+D5+77+50+6+0+64+AC+81+EA+A6+97+BD+FF+44+41+EA+47+80+82+");
            driver = new Augmenter().augment(driver);
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            System.out.println("File:" + srcFile);
            //FileUtils.copyFile(srcFile, new File("c:\\tmp\\screenshot.png"));
        }
        driver.quit();
        System.out.println("Fim: "+(new Date()).toString());
        
    }

}
