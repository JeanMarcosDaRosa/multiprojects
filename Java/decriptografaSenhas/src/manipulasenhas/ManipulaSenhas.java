/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulasenhas;

import java.util.Scanner;

/**
 *
 * @author Luis Paulo
 */
public class ManipulaSenhas {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o numero da opcao desejada: 1 - Criptografar, 2 - Decriptografar, 3 - Sair");
        Integer opcao = sc.nextInt();
        while (opcao != 3) {
            if (opcao == 1) {
                System.out.println("Informe a senha original:");
                String senha = sc.next();
                System.out.println("Informe o tipo de criptografia (1 ou 2):");
                Integer tipoCript = sc.nextInt();

                if (tipoCript == 1) {
                    try {
                        String senhaCript = PWSeq.encrypt(senha);
                        System.out.println("Senha Criptografada:" + senhaCript);
                    } catch (Exception e) {
                        System.out.println("" + e.getMessage());
                    }
                } else if (tipoCript == 2) {
                    try {
                        String senhaCript = CryptoUtil.getInstance().encrypt(senha);
                        System.out.println("Senha Criptografada:" + senhaCript);
                    } catch (Exception e) {
                        System.out.println("" + e.getMessage());
                    }
                } else {
                    System.out.println("Tipo de Criptografia nao mapeado!!");
                }

            } else if (opcao == 2) {
                System.out.println("Informe a senha criptografada:");
                String senhaCript = sc.next();
                System.out.println("Informe o tipo de criptografia (1 ou 2):");
                Integer tipoCript = sc.nextInt();

                if (tipoCript == 1) {
                    try {
                        String senha = PWSeq.decrypt(senhaCript);
                        System.out.println("Senha Original:" + senha);
                    } catch (Exception e) {
                        System.out.println("" + e.getMessage());
                    }

                } else if (tipoCript == 2) {
                    try {
                        String senha = CryptoUtil.getInstance().decrypt(senhaCript);
                        System.out.println("Senha Original:" + senha);
                    } catch (Exception e) {
                        System.out.println("" + e.getMessage());
                    }

                } else {
                    System.out.println("Tipo de Criptografia nao mapeado!!");
                }

            } else {
                System.out.println("Opcao Invalida!");
            }
            System.out.println("Digite o numero da opcao desejada: 1 - Criptografar, 2 - Decriptografar, 3 - Sair");
            opcao = sc.nextInt();
        }

        
    }
}
