package automate;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

public class DesktopIteract {

    JTextField txtPedido = new JTextField();
    JTextField txtChave = new JTextField();
    JTextField txtPerfil = new JTextField();
    JTextField txtBdu;
    String pedido;
    String chave;
    String perfil;
    String bdu;

    public void pedido() throws AWTException {
        Robot rb = new Robot();
        rb.setAutoDelay(100);
        rb.mouseMove(0, 0);
        rb.mouseMove(400, 980);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(550, 250);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(250, 305);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(550, 535);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(50, 535);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(60, 170);
        rb.mousePress(16);
        rb.mousePress(16);
        rb.mousePress(16);
        rb.mouseRelease(16);

        rb.keyPress(17);
        rb.keyPress(67);
        rb.keyRelease(67);
        rb.keyRelease(17);

        this.txtPedido.paste();
        rb.delay(2000);
        this.pedido = this.txtPedido.getText();
        System.out.println("Pedido: " + this.pedido);
        rb.mouseMove(150, 132);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(700, 270);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(150, 130);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(430, 230);
        rb.mousePress(16);
        rb.mousePress(16);
        rb.mousePress(16);
        rb.mouseRelease(16);

        rb.keyPress(17);
        rb.keyPress(67);
        rb.keyRelease(67);
        rb.keyRelease(17);

        this.txtChave.paste();
        rb.delay(2000);
        this.chave = this.txtChave.getText();
        System.out.println("Chave: " + this.chave);
        rb.mouseMove(60, 335);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.keyPress(35);
        rb.mousePress(16);
        rb.mousePress(16);
        rb.mouseRelease(16);

        rb.keyPress(17);
        rb.keyPress(67);
        rb.keyRelease(67);
        rb.keyRelease(17);

        this.txtPerfil.paste();
        rb.delay(2000);
        this.perfil = this.txtPerfil.getText();
        System.out.println("Perfil: " + this.perfil);

        rb.mouseMove(700, 980);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(40, 400);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(440, 275);
        rb.mousePress(16);
        rb.mouseRelease(16);
        Clipboard teclado = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection selecao = new StringSelection(this.chave);
        teclado.setContents(selecao, null);

        rb.keyPress(17);
        rb.keyPress(86);
        rb.keyRelease(86);
        rb.keyRelease(17);

        rb.delay(300);
        rb.keyPress(10);
        rb.mouseMove(365, 325);
        rb.delay(300);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.mouseMove(283, 287);
        rb.mousePress(16);
        rb.mouseRelease(16);
        rb.delay(2000);
        rb.keyPress(17);
        rb.keyPress(65);
        rb.keyRelease(65);
        rb.keyRelease(17);
        rb.delay(300);
        rb.keyPress(17);
        rb.keyPress(67);
        rb.keyRelease(67);
        rb.keyRelease(17);
        this.txtBdu = new JTextField();
        this.txtBdu.paste();
        rb.delay(2000);
        this.bdu = this.txtBdu.getText();
        System.out.println("BDU: " + this.bdu);
    }

    public static void main(String[] args) {
        DesktopIteract o = new DesktopIteract();
        try {
            o.pedido();
        } catch (AWTException ex) {
            Logger.getLogger(DesktopIteract.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
