/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tn3.util;

/**
 *
 * @author mateusbaruffi
 */
import org.apache.axis.wsdl.WSDL2Java;

public class WSDLGen {
    
    

    public static void main(String[] args) {
        WSDLGen gen = new WSDLGen();

        try {
            System.out.println("Gerando WSDL...");
            String servico_rs = "C:\\Users\\jean.marcos\\Desktop\\CIOT\\Apisul\\roteirizador.wsdl";
            //String servico_rs = "C:\\wsdl\\CteRecepcao_.wsdl";
            
            gen.generate(servico_rs, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void generate(String wsdl, String pack) {

        try {
            
            
            WSDL2Java.main(new String[]{wsdl, "-o", "src", "-p", "com.tn3.reteirizador.axis.base" + ((pack != null && !pack.equals("")) ? "." + pack : ""), "-d",  "xmlbeans",  "-s"});
            
            //WSDL2Java.main(new String[]{wsdl, "-o", "src", "-p", "com.tn3.cte.cliente.teste.axis" + ((pack != null && !pack.equals("")) ? "." + pack : ""), "-d",  "xmlbeans",  "-s"});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("*** Geração concluída ***");
    }
}