/**
 * RoteirizadorServiceSoap_BindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class RoteirizadorServiceSoap_BindingImpl implements com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType{
    public com.tn3.reteirizador.axis.base.RotaDetalhada buscarRota(java.lang.String codigoTransportadora, java.lang.String nomeRota) throws java.rmi.RemoteException {
        return null;
    }

    public com.tn3.reteirizador.axis.base.RotaDetalhada criarRotaPermanente(com.tn3.reteirizador.axis.base.DeclaracaoRota declararRota) throws java.rmi.RemoteException {
        return null;
    }

    public com.tn3.reteirizador.axis.base.RotaDetalhada criarRotaTemporaria(com.tn3.reteirizador.axis.base.DeclaracaoRota declararRota) throws java.rmi.RemoteException {
        return null;
    }

}
