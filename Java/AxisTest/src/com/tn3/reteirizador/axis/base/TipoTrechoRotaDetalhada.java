/**
 * TipoTrechoRotaDetalhada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class TipoTrechoRotaDetalhada implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoTrechoRotaDetalhada(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Terra = "Terra";
    public static final java.lang.String _Balsa = "Balsa";
    public static final java.lang.String _PistaSimples = "PistaSimples";
    public static final java.lang.String _Pavimentacao = "Pavimentacao";
    public static final java.lang.String _MasCondicoes = "MasCondicoes";
    public static final java.lang.String _Duplicacao = "Duplicacao";
    public static final java.lang.String _PistaDupla = "PistaDupla";
    public static final TipoTrechoRotaDetalhada Terra = new TipoTrechoRotaDetalhada(_Terra);
    public static final TipoTrechoRotaDetalhada Balsa = new TipoTrechoRotaDetalhada(_Balsa);
    public static final TipoTrechoRotaDetalhada PistaSimples = new TipoTrechoRotaDetalhada(_PistaSimples);
    public static final TipoTrechoRotaDetalhada Pavimentacao = new TipoTrechoRotaDetalhada(_Pavimentacao);
    public static final TipoTrechoRotaDetalhada MasCondicoes = new TipoTrechoRotaDetalhada(_MasCondicoes);
    public static final TipoTrechoRotaDetalhada Duplicacao = new TipoTrechoRotaDetalhada(_Duplicacao);
    public static final TipoTrechoRotaDetalhada PistaDupla = new TipoTrechoRotaDetalhada(_PistaDupla);
    public java.lang.String getValue() { return _value_;}
    public static TipoTrechoRotaDetalhada fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoTrechoRotaDetalhada enumeration = (TipoTrechoRotaDetalhada)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoTrechoRotaDetalhada fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoTrechoRotaDetalhada.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "TipoTrechoRotaDetalhada"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
