/**
 * ResumoTrechoRotaDetalhada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class ResumoTrechoRotaDetalhada  implements java.io.Serializable {
    private com.tn3.reteirizador.axis.base.TipoTrechoRotaDetalhada tipoPista;

    private double distancia;

    public ResumoTrechoRotaDetalhada() {
    }

    public ResumoTrechoRotaDetalhada(
           com.tn3.reteirizador.axis.base.TipoTrechoRotaDetalhada tipoPista,
           double distancia) {
           this.tipoPista = tipoPista;
           this.distancia = distancia;
    }


    /**
     * Gets the tipoPista value for this ResumoTrechoRotaDetalhada.
     * 
     * @return tipoPista
     */
    public com.tn3.reteirizador.axis.base.TipoTrechoRotaDetalhada getTipoPista() {
        return tipoPista;
    }


    /**
     * Sets the tipoPista value for this ResumoTrechoRotaDetalhada.
     * 
     * @param tipoPista
     */
    public void setTipoPista(com.tn3.reteirizador.axis.base.TipoTrechoRotaDetalhada tipoPista) {
        this.tipoPista = tipoPista;
    }


    /**
     * Gets the distancia value for this ResumoTrechoRotaDetalhada.
     * 
     * @return distancia
     */
    public double getDistancia() {
        return distancia;
    }


    /**
     * Sets the distancia value for this ResumoTrechoRotaDetalhada.
     * 
     * @param distancia
     */
    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResumoTrechoRotaDetalhada)) return false;
        ResumoTrechoRotaDetalhada other = (ResumoTrechoRotaDetalhada) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipoPista==null && other.getTipoPista()==null) || 
             (this.tipoPista!=null &&
              this.tipoPista.equals(other.getTipoPista()))) &&
            this.distancia == other.getDistancia();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipoPista() != null) {
            _hashCode += getTipoPista().hashCode();
        }
        _hashCode += new Double(getDistancia()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResumoTrechoRotaDetalhada.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoTrechoRotaDetalhada"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPista");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TipoPista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "TipoTrechoRotaDetalhada"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distancia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Distancia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
