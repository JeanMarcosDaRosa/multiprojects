/**
 * ParadaRotaDetalhada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class ParadaRotaDetalhada  implements java.io.Serializable {
    private java.lang.String cepParada;

    private java.lang.String nomeEstadoParada;

    private java.lang.String UFEstadoParada;

    private java.lang.String nomeCidadeParada;

    private java.lang.String logradouroParada;

    public ParadaRotaDetalhada() {
    }

    public ParadaRotaDetalhada(
           java.lang.String cepParada,
           java.lang.String nomeEstadoParada,
           java.lang.String UFEstadoParada,
           java.lang.String nomeCidadeParada,
           java.lang.String logradouroParada) {
           this.cepParada = cepParada;
           this.nomeEstadoParada = nomeEstadoParada;
           this.UFEstadoParada = UFEstadoParada;
           this.nomeCidadeParada = nomeCidadeParada;
           this.logradouroParada = logradouroParada;
    }


    /**
     * Gets the cepParada value for this ParadaRotaDetalhada.
     * 
     * @return cepParada
     */
    public java.lang.String getCepParada() {
        return cepParada;
    }


    /**
     * Sets the cepParada value for this ParadaRotaDetalhada.
     * 
     * @param cepParada
     */
    public void setCepParada(java.lang.String cepParada) {
        this.cepParada = cepParada;
    }


    /**
     * Gets the nomeEstadoParada value for this ParadaRotaDetalhada.
     * 
     * @return nomeEstadoParada
     */
    public java.lang.String getNomeEstadoParada() {
        return nomeEstadoParada;
    }


    /**
     * Sets the nomeEstadoParada value for this ParadaRotaDetalhada.
     * 
     * @param nomeEstadoParada
     */
    public void setNomeEstadoParada(java.lang.String nomeEstadoParada) {
        this.nomeEstadoParada = nomeEstadoParada;
    }


    /**
     * Gets the UFEstadoParada value for this ParadaRotaDetalhada.
     * 
     * @return UFEstadoParada
     */
    public java.lang.String getUFEstadoParada() {
        return UFEstadoParada;
    }


    /**
     * Sets the UFEstadoParada value for this ParadaRotaDetalhada.
     * 
     * @param UFEstadoParada
     */
    public void setUFEstadoParada(java.lang.String UFEstadoParada) {
        this.UFEstadoParada = UFEstadoParada;
    }


    /**
     * Gets the nomeCidadeParada value for this ParadaRotaDetalhada.
     * 
     * @return nomeCidadeParada
     */
    public java.lang.String getNomeCidadeParada() {
        return nomeCidadeParada;
    }


    /**
     * Sets the nomeCidadeParada value for this ParadaRotaDetalhada.
     * 
     * @param nomeCidadeParada
     */
    public void setNomeCidadeParada(java.lang.String nomeCidadeParada) {
        this.nomeCidadeParada = nomeCidadeParada;
    }


    /**
     * Gets the logradouroParada value for this ParadaRotaDetalhada.
     * 
     * @return logradouroParada
     */
    public java.lang.String getLogradouroParada() {
        return logradouroParada;
    }


    /**
     * Sets the logradouroParada value for this ParadaRotaDetalhada.
     * 
     * @param logradouroParada
     */
    public void setLogradouroParada(java.lang.String logradouroParada) {
        this.logradouroParada = logradouroParada;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ParadaRotaDetalhada)) return false;
        ParadaRotaDetalhada other = (ParadaRotaDetalhada) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cepParada==null && other.getCepParada()==null) || 
             (this.cepParada!=null &&
              this.cepParada.equals(other.getCepParada()))) &&
            ((this.nomeEstadoParada==null && other.getNomeEstadoParada()==null) || 
             (this.nomeEstadoParada!=null &&
              this.nomeEstadoParada.equals(other.getNomeEstadoParada()))) &&
            ((this.UFEstadoParada==null && other.getUFEstadoParada()==null) || 
             (this.UFEstadoParada!=null &&
              this.UFEstadoParada.equals(other.getUFEstadoParada()))) &&
            ((this.nomeCidadeParada==null && other.getNomeCidadeParada()==null) || 
             (this.nomeCidadeParada!=null &&
              this.nomeCidadeParada.equals(other.getNomeCidadeParada()))) &&
            ((this.logradouroParada==null && other.getLogradouroParada()==null) || 
             (this.logradouroParada!=null &&
              this.logradouroParada.equals(other.getLogradouroParada())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCepParada() != null) {
            _hashCode += getCepParada().hashCode();
        }
        if (getNomeEstadoParada() != null) {
            _hashCode += getNomeEstadoParada().hashCode();
        }
        if (getUFEstadoParada() != null) {
            _hashCode += getUFEstadoParada().hashCode();
        }
        if (getNomeCidadeParada() != null) {
            _hashCode += getNomeCidadeParada().hashCode();
        }
        if (getLogradouroParada() != null) {
            _hashCode += getLogradouroParada().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ParadaRotaDetalhada.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ParadaRotaDetalhada"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepParada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CepParada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeEstadoParada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NomeEstadoParada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UFEstadoParada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "UFEstadoParada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeCidadeParada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NomeCidadeParada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logradouroParada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "LogradouroParada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
