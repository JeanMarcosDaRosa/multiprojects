/**
 * RotaDetalhada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class RotaDetalhada  implements java.io.Serializable {
    private int idRota;

    private java.lang.String nomeRota;

    private java.lang.String descricaoRota;

    private int idTransportadora;

    private com.tn3.reteirizador.axis.base.Endereco origem;

    private com.tn3.reteirizador.axis.base.Endereco destino;

    private com.tn3.reteirizador.axis.base.ParadaRotaDetalhada[] paradas;

    private com.tn3.reteirizador.axis.base.TipoRotaDetalhada tipoRota;

    private com.tn3.reteirizador.axis.base.CategoriaVeiculoRotaDetalhada categoriaVeiculo;

    private com.tn3.reteirizador.axis.base.ResumoRotaDetalhada resumoRota;

    private com.tn3.reteirizador.axis.base.Pedagio[] resumoPedagio;

    private double totalPedagio;

    private com.tn3.reteirizador.axis.base.ResumoTrechoRotaDetalhada[] resumoTrechos;

    private boolean sucesso;

    private java.lang.String mensagem;

    private com.tn3.reteirizador.axis.base.Excecao excecao;

    public RotaDetalhada() {
    }

    public RotaDetalhada(
           int idRota,
           java.lang.String nomeRota,
           java.lang.String descricaoRota,
           int idTransportadora,
           com.tn3.reteirizador.axis.base.Endereco origem,
           com.tn3.reteirizador.axis.base.Endereco destino,
           com.tn3.reteirizador.axis.base.ParadaRotaDetalhada[] paradas,
           com.tn3.reteirizador.axis.base.TipoRotaDetalhada tipoRota,
           com.tn3.reteirizador.axis.base.CategoriaVeiculoRotaDetalhada categoriaVeiculo,
           com.tn3.reteirizador.axis.base.ResumoRotaDetalhada resumoRota,
           com.tn3.reteirizador.axis.base.Pedagio[] resumoPedagio,
           double totalPedagio,
           com.tn3.reteirizador.axis.base.ResumoTrechoRotaDetalhada[] resumoTrechos,
           boolean sucesso,
           java.lang.String mensagem,
           com.tn3.reteirizador.axis.base.Excecao excecao) {
           this.idRota = idRota;
           this.nomeRota = nomeRota;
           this.descricaoRota = descricaoRota;
           this.idTransportadora = idTransportadora;
           this.origem = origem;
           this.destino = destino;
           this.paradas = paradas;
           this.tipoRota = tipoRota;
           this.categoriaVeiculo = categoriaVeiculo;
           this.resumoRota = resumoRota;
           this.resumoPedagio = resumoPedagio;
           this.totalPedagio = totalPedagio;
           this.resumoTrechos = resumoTrechos;
           this.sucesso = sucesso;
           this.mensagem = mensagem;
           this.excecao = excecao;
    }


    /**
     * Gets the idRota value for this RotaDetalhada.
     * 
     * @return idRota
     */
    public int getIdRota() {
        return idRota;
    }


    /**
     * Sets the idRota value for this RotaDetalhada.
     * 
     * @param idRota
     */
    public void setIdRota(int idRota) {
        this.idRota = idRota;
    }


    /**
     * Gets the nomeRota value for this RotaDetalhada.
     * 
     * @return nomeRota
     */
    public java.lang.String getNomeRota() {
        return nomeRota;
    }


    /**
     * Sets the nomeRota value for this RotaDetalhada.
     * 
     * @param nomeRota
     */
    public void setNomeRota(java.lang.String nomeRota) {
        this.nomeRota = nomeRota;
    }


    /**
     * Gets the descricaoRota value for this RotaDetalhada.
     * 
     * @return descricaoRota
     */
    public java.lang.String getDescricaoRota() {
        return descricaoRota;
    }


    /**
     * Sets the descricaoRota value for this RotaDetalhada.
     * 
     * @param descricaoRota
     */
    public void setDescricaoRota(java.lang.String descricaoRota) {
        this.descricaoRota = descricaoRota;
    }


    /**
     * Gets the idTransportadora value for this RotaDetalhada.
     * 
     * @return idTransportadora
     */
    public int getIdTransportadora() {
        return idTransportadora;
    }


    /**
     * Sets the idTransportadora value for this RotaDetalhada.
     * 
     * @param idTransportadora
     */
    public void setIdTransportadora(int idTransportadora) {
        this.idTransportadora = idTransportadora;
    }


    /**
     * Gets the origem value for this RotaDetalhada.
     * 
     * @return origem
     */
    public com.tn3.reteirizador.axis.base.Endereco getOrigem() {
        return origem;
    }


    /**
     * Sets the origem value for this RotaDetalhada.
     * 
     * @param origem
     */
    public void setOrigem(com.tn3.reteirizador.axis.base.Endereco origem) {
        this.origem = origem;
    }


    /**
     * Gets the destino value for this RotaDetalhada.
     * 
     * @return destino
     */
    public com.tn3.reteirizador.axis.base.Endereco getDestino() {
        return destino;
    }


    /**
     * Sets the destino value for this RotaDetalhada.
     * 
     * @param destino
     */
    public void setDestino(com.tn3.reteirizador.axis.base.Endereco destino) {
        this.destino = destino;
    }


    /**
     * Gets the paradas value for this RotaDetalhada.
     * 
     * @return paradas
     */
    public com.tn3.reteirizador.axis.base.ParadaRotaDetalhada[] getParadas() {
        return paradas;
    }


    /**
     * Sets the paradas value for this RotaDetalhada.
     * 
     * @param paradas
     */
    public void setParadas(com.tn3.reteirizador.axis.base.ParadaRotaDetalhada[] paradas) {
        this.paradas = paradas;
    }


    /**
     * Gets the tipoRota value for this RotaDetalhada.
     * 
     * @return tipoRota
     */
    public com.tn3.reteirizador.axis.base.TipoRotaDetalhada getTipoRota() {
        return tipoRota;
    }


    /**
     * Sets the tipoRota value for this RotaDetalhada.
     * 
     * @param tipoRota
     */
    public void setTipoRota(com.tn3.reteirizador.axis.base.TipoRotaDetalhada tipoRota) {
        this.tipoRota = tipoRota;
    }


    /**
     * Gets the categoriaVeiculo value for this RotaDetalhada.
     * 
     * @return categoriaVeiculo
     */
    public com.tn3.reteirizador.axis.base.CategoriaVeiculoRotaDetalhada getCategoriaVeiculo() {
        return categoriaVeiculo;
    }


    /**
     * Sets the categoriaVeiculo value for this RotaDetalhada.
     * 
     * @param categoriaVeiculo
     */
    public void setCategoriaVeiculo(com.tn3.reteirizador.axis.base.CategoriaVeiculoRotaDetalhada categoriaVeiculo) {
        this.categoriaVeiculo = categoriaVeiculo;
    }


    /**
     * Gets the resumoRota value for this RotaDetalhada.
     * 
     * @return resumoRota
     */
    public com.tn3.reteirizador.axis.base.ResumoRotaDetalhada getResumoRota() {
        return resumoRota;
    }


    /**
     * Sets the resumoRota value for this RotaDetalhada.
     * 
     * @param resumoRota
     */
    public void setResumoRota(com.tn3.reteirizador.axis.base.ResumoRotaDetalhada resumoRota) {
        this.resumoRota = resumoRota;
    }


    /**
     * Gets the resumoPedagio value for this RotaDetalhada.
     * 
     * @return resumoPedagio
     */
    public com.tn3.reteirizador.axis.base.Pedagio[] getResumoPedagio() {
        return resumoPedagio;
    }


    /**
     * Sets the resumoPedagio value for this RotaDetalhada.
     * 
     * @param resumoPedagio
     */
    public void setResumoPedagio(com.tn3.reteirizador.axis.base.Pedagio[] resumoPedagio) {
        this.resumoPedagio = resumoPedagio;
    }


    /**
     * Gets the totalPedagio value for this RotaDetalhada.
     * 
     * @return totalPedagio
     */
    public double getTotalPedagio() {
        return totalPedagio;
    }


    /**
     * Sets the totalPedagio value for this RotaDetalhada.
     * 
     * @param totalPedagio
     */
    public void setTotalPedagio(double totalPedagio) {
        this.totalPedagio = totalPedagio;
    }


    /**
     * Gets the resumoTrechos value for this RotaDetalhada.
     * 
     * @return resumoTrechos
     */
    public com.tn3.reteirizador.axis.base.ResumoTrechoRotaDetalhada[] getResumoTrechos() {
        return resumoTrechos;
    }


    /**
     * Sets the resumoTrechos value for this RotaDetalhada.
     * 
     * @param resumoTrechos
     */
    public void setResumoTrechos(com.tn3.reteirizador.axis.base.ResumoTrechoRotaDetalhada[] resumoTrechos) {
        this.resumoTrechos = resumoTrechos;
    }


    /**
     * Gets the sucesso value for this RotaDetalhada.
     * 
     * @return sucesso
     */
    public boolean isSucesso() {
        return sucesso;
    }


    /**
     * Sets the sucesso value for this RotaDetalhada.
     * 
     * @param sucesso
     */
    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }


    /**
     * Gets the mensagem value for this RotaDetalhada.
     * 
     * @return mensagem
     */
    public java.lang.String getMensagem() {
        return mensagem;
    }


    /**
     * Sets the mensagem value for this RotaDetalhada.
     * 
     * @param mensagem
     */
    public void setMensagem(java.lang.String mensagem) {
        this.mensagem = mensagem;
    }


    /**
     * Gets the excecao value for this RotaDetalhada.
     * 
     * @return excecao
     */
    public com.tn3.reteirizador.axis.base.Excecao getExcecao() {
        return excecao;
    }


    /**
     * Sets the excecao value for this RotaDetalhada.
     * 
     * @param excecao
     */
    public void setExcecao(com.tn3.reteirizador.axis.base.Excecao excecao) {
        this.excecao = excecao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RotaDetalhada)) return false;
        RotaDetalhada other = (RotaDetalhada) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.idRota == other.getIdRota() &&
            ((this.nomeRota==null && other.getNomeRota()==null) || 
             (this.nomeRota!=null &&
              this.nomeRota.equals(other.getNomeRota()))) &&
            ((this.descricaoRota==null && other.getDescricaoRota()==null) || 
             (this.descricaoRota!=null &&
              this.descricaoRota.equals(other.getDescricaoRota()))) &&
            this.idTransportadora == other.getIdTransportadora() &&
            ((this.origem==null && other.getOrigem()==null) || 
             (this.origem!=null &&
              this.origem.equals(other.getOrigem()))) &&
            ((this.destino==null && other.getDestino()==null) || 
             (this.destino!=null &&
              this.destino.equals(other.getDestino()))) &&
            ((this.paradas==null && other.getParadas()==null) || 
             (this.paradas!=null &&
              java.util.Arrays.equals(this.paradas, other.getParadas()))) &&
            ((this.tipoRota==null && other.getTipoRota()==null) || 
             (this.tipoRota!=null &&
              this.tipoRota.equals(other.getTipoRota()))) &&
            ((this.categoriaVeiculo==null && other.getCategoriaVeiculo()==null) || 
             (this.categoriaVeiculo!=null &&
              this.categoriaVeiculo.equals(other.getCategoriaVeiculo()))) &&
            ((this.resumoRota==null && other.getResumoRota()==null) || 
             (this.resumoRota!=null &&
              this.resumoRota.equals(other.getResumoRota()))) &&
            ((this.resumoPedagio==null && other.getResumoPedagio()==null) || 
             (this.resumoPedagio!=null &&
              java.util.Arrays.equals(this.resumoPedagio, other.getResumoPedagio()))) &&
            this.totalPedagio == other.getTotalPedagio() &&
            ((this.resumoTrechos==null && other.getResumoTrechos()==null) || 
             (this.resumoTrechos!=null &&
              java.util.Arrays.equals(this.resumoTrechos, other.getResumoTrechos()))) &&
            this.sucesso == other.isSucesso() &&
            ((this.mensagem==null && other.getMensagem()==null) || 
             (this.mensagem!=null &&
              this.mensagem.equals(other.getMensagem()))) &&
            ((this.excecao==null && other.getExcecao()==null) || 
             (this.excecao!=null &&
              this.excecao.equals(other.getExcecao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getIdRota();
        if (getNomeRota() != null) {
            _hashCode += getNomeRota().hashCode();
        }
        if (getDescricaoRota() != null) {
            _hashCode += getDescricaoRota().hashCode();
        }
        _hashCode += getIdTransportadora();
        if (getOrigem() != null) {
            _hashCode += getOrigem().hashCode();
        }
        if (getDestino() != null) {
            _hashCode += getDestino().hashCode();
        }
        if (getParadas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParadas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParadas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipoRota() != null) {
            _hashCode += getTipoRota().hashCode();
        }
        if (getCategoriaVeiculo() != null) {
            _hashCode += getCategoriaVeiculo().hashCode();
        }
        if (getResumoRota() != null) {
            _hashCode += getResumoRota().hashCode();
        }
        if (getResumoPedagio() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResumoPedagio());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResumoPedagio(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Double(getTotalPedagio()).hashCode();
        if (getResumoTrechos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResumoTrechos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResumoTrechos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isSucesso() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getMensagem() != null) {
            _hashCode += getMensagem().hashCode();
        }
        if (getExcecao() != null) {
            _hashCode += getExcecao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RotaDetalhada.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "RotaDetalhada"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NomeRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DescricaoRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTransportadora");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdTransportadora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Origem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Destino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Endereco"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paradas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Paradas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ParadaRotaDetalhada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "ParadaRotaDetalhada"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TipoRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "TipoRotaDetalhada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoriaVeiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CategoriaVeiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CategoriaVeiculoRotaDetalhada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resumoRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoRotaDetalhada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resumoPedagio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoPedagio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Pedagio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "Pedagio"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPedagio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TotalPedagio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resumoTrechos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoTrechos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoTrechoRotaDetalhada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoTrechoRotaDetalhada"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sucesso");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Sucesso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Mensagem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excecao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Excecao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Excecao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
