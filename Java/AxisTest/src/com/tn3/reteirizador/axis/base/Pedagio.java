/**
 * Pedagio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class Pedagio  implements java.io.Serializable {
    private java.lang.String nome;

    private java.lang.String endereco;

    private java.lang.String direcao;

    private java.lang.String concessionaria;

    private java.lang.String telefone;

    private java.lang.String estado;

    private double taxa;

    private double taxaPorEixo;

    public Pedagio() {
    }

    public Pedagio(
           java.lang.String nome,
           java.lang.String endereco,
           java.lang.String direcao,
           java.lang.String concessionaria,
           java.lang.String telefone,
           java.lang.String estado,
           double taxa,
           double taxaPorEixo) {
           this.nome = nome;
           this.endereco = endereco;
           this.direcao = direcao;
           this.concessionaria = concessionaria;
           this.telefone = telefone;
           this.estado = estado;
           this.taxa = taxa;
           this.taxaPorEixo = taxaPorEixo;
    }


    /**
     * Gets the nome value for this Pedagio.
     * 
     * @return nome
     */
    public java.lang.String getNome() {
        return nome;
    }


    /**
     * Sets the nome value for this Pedagio.
     * 
     * @param nome
     */
    public void setNome(java.lang.String nome) {
        this.nome = nome;
    }


    /**
     * Gets the endereco value for this Pedagio.
     * 
     * @return endereco
     */
    public java.lang.String getEndereco() {
        return endereco;
    }


    /**
     * Sets the endereco value for this Pedagio.
     * 
     * @param endereco
     */
    public void setEndereco(java.lang.String endereco) {
        this.endereco = endereco;
    }


    /**
     * Gets the direcao value for this Pedagio.
     * 
     * @return direcao
     */
    public java.lang.String getDirecao() {
        return direcao;
    }


    /**
     * Sets the direcao value for this Pedagio.
     * 
     * @param direcao
     */
    public void setDirecao(java.lang.String direcao) {
        this.direcao = direcao;
    }


    /**
     * Gets the concessionaria value for this Pedagio.
     * 
     * @return concessionaria
     */
    public java.lang.String getConcessionaria() {
        return concessionaria;
    }


    /**
     * Sets the concessionaria value for this Pedagio.
     * 
     * @param concessionaria
     */
    public void setConcessionaria(java.lang.String concessionaria) {
        this.concessionaria = concessionaria;
    }


    /**
     * Gets the telefone value for this Pedagio.
     * 
     * @return telefone
     */
    public java.lang.String getTelefone() {
        return telefone;
    }


    /**
     * Sets the telefone value for this Pedagio.
     * 
     * @param telefone
     */
    public void setTelefone(java.lang.String telefone) {
        this.telefone = telefone;
    }


    /**
     * Gets the estado value for this Pedagio.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this Pedagio.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }


    /**
     * Gets the taxa value for this Pedagio.
     * 
     * @return taxa
     */
    public double getTaxa() {
        return taxa;
    }


    /**
     * Sets the taxa value for this Pedagio.
     * 
     * @param taxa
     */
    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }


    /**
     * Gets the taxaPorEixo value for this Pedagio.
     * 
     * @return taxaPorEixo
     */
    public double getTaxaPorEixo() {
        return taxaPorEixo;
    }


    /**
     * Sets the taxaPorEixo value for this Pedagio.
     * 
     * @param taxaPorEixo
     */
    public void setTaxaPorEixo(double taxaPorEixo) {
        this.taxaPorEixo = taxaPorEixo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pedagio)) return false;
        Pedagio other = (Pedagio) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nome==null && other.getNome()==null) || 
             (this.nome!=null &&
              this.nome.equals(other.getNome()))) &&
            ((this.endereco==null && other.getEndereco()==null) || 
             (this.endereco!=null &&
              this.endereco.equals(other.getEndereco()))) &&
            ((this.direcao==null && other.getDirecao()==null) || 
             (this.direcao!=null &&
              this.direcao.equals(other.getDirecao()))) &&
            ((this.concessionaria==null && other.getConcessionaria()==null) || 
             (this.concessionaria!=null &&
              this.concessionaria.equals(other.getConcessionaria()))) &&
            ((this.telefone==null && other.getTelefone()==null) || 
             (this.telefone!=null &&
              this.telefone.equals(other.getTelefone()))) &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado()))) &&
            this.taxa == other.getTaxa() &&
            this.taxaPorEixo == other.getTaxaPorEixo();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNome() != null) {
            _hashCode += getNome().hashCode();
        }
        if (getEndereco() != null) {
            _hashCode += getEndereco().hashCode();
        }
        if (getDirecao() != null) {
            _hashCode += getDirecao().hashCode();
        }
        if (getConcessionaria() != null) {
            _hashCode += getConcessionaria().hashCode();
        }
        if (getTelefone() != null) {
            _hashCode += getTelefone().hashCode();
        }
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        _hashCode += new Double(getTaxa()).hashCode();
        _hashCode += new Double(getTaxaPorEixo()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pedagio.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Pedagio"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Nome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endereco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Endereco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direcao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Direcao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("concessionaria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Concessionaria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Telefone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Taxa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxaPorEixo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TaxaPorEixo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
