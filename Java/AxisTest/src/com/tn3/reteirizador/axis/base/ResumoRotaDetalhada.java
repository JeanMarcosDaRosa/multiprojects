/**
 * ResumoRotaDetalhada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class ResumoRotaDetalhada  implements java.io.Serializable {
    private java.lang.String distancia;

    private java.lang.String tempoPrevisto;

    private java.lang.String combustivelPrevisto;

    public ResumoRotaDetalhada() {
    }

    public ResumoRotaDetalhada(
           java.lang.String distancia,
           java.lang.String tempoPrevisto,
           java.lang.String combustivelPrevisto) {
           this.distancia = distancia;
           this.tempoPrevisto = tempoPrevisto;
           this.combustivelPrevisto = combustivelPrevisto;
    }


    /**
     * Gets the distancia value for this ResumoRotaDetalhada.
     * 
     * @return distancia
     */
    public java.lang.String getDistancia() {
        return distancia;
    }


    /**
     * Sets the distancia value for this ResumoRotaDetalhada.
     * 
     * @param distancia
     */
    public void setDistancia(java.lang.String distancia) {
        this.distancia = distancia;
    }


    /**
     * Gets the tempoPrevisto value for this ResumoRotaDetalhada.
     * 
     * @return tempoPrevisto
     */
    public java.lang.String getTempoPrevisto() {
        return tempoPrevisto;
    }


    /**
     * Sets the tempoPrevisto value for this ResumoRotaDetalhada.
     * 
     * @param tempoPrevisto
     */
    public void setTempoPrevisto(java.lang.String tempoPrevisto) {
        this.tempoPrevisto = tempoPrevisto;
    }


    /**
     * Gets the combustivelPrevisto value for this ResumoRotaDetalhada.
     * 
     * @return combustivelPrevisto
     */
    public java.lang.String getCombustivelPrevisto() {
        return combustivelPrevisto;
    }


    /**
     * Sets the combustivelPrevisto value for this ResumoRotaDetalhada.
     * 
     * @param combustivelPrevisto
     */
    public void setCombustivelPrevisto(java.lang.String combustivelPrevisto) {
        this.combustivelPrevisto = combustivelPrevisto;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResumoRotaDetalhada)) return false;
        ResumoRotaDetalhada other = (ResumoRotaDetalhada) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.distancia==null && other.getDistancia()==null) || 
             (this.distancia!=null &&
              this.distancia.equals(other.getDistancia()))) &&
            ((this.tempoPrevisto==null && other.getTempoPrevisto()==null) || 
             (this.tempoPrevisto!=null &&
              this.tempoPrevisto.equals(other.getTempoPrevisto()))) &&
            ((this.combustivelPrevisto==null && other.getCombustivelPrevisto()==null) || 
             (this.combustivelPrevisto!=null &&
              this.combustivelPrevisto.equals(other.getCombustivelPrevisto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDistancia() != null) {
            _hashCode += getDistancia().hashCode();
        }
        if (getTempoPrevisto() != null) {
            _hashCode += getTempoPrevisto().hashCode();
        }
        if (getCombustivelPrevisto() != null) {
            _hashCode += getCombustivelPrevisto().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResumoRotaDetalhada.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ResumoRotaDetalhada"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distancia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Distancia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tempoPrevisto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TempoPrevisto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("combustivelPrevisto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CombustivelPrevisto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
