/**
 * RoteirizadorServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class RoteirizadorServiceLocator extends org.apache.axis.client.Service implements com.tn3.reteirizador.axis.base.RoteirizadorService {

    public RoteirizadorServiceLocator() {
    }


    public RoteirizadorServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RoteirizadorServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RoteirizadorServiceSoap12
    private java.lang.String RoteirizadorServiceSoap12_address = "https://hml.apisulcard.com.br/webservice/roteirizadorservice.asmx";

    public java.lang.String getRoteirizadorServiceSoap12Address() {
        return RoteirizadorServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RoteirizadorServiceSoap12WSDDServiceName = "RoteirizadorServiceSoap12";

    public java.lang.String getRoteirizadorServiceSoap12WSDDServiceName() {
        return RoteirizadorServiceSoap12WSDDServiceName;
    }

    public void setRoteirizadorServiceSoap12WSDDServiceName(java.lang.String name) {
        RoteirizadorServiceSoap12WSDDServiceName = name;
    }

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RoteirizadorServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRoteirizadorServiceSoap12(endpoint);
    }

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap12Stub _stub = new com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getRoteirizadorServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRoteirizadorServiceSoap12EndpointAddress(java.lang.String address) {
        RoteirizadorServiceSoap12_address = address;
    }


    // Use to get a proxy class for RoteirizadorServiceSoap
    private java.lang.String RoteirizadorServiceSoap_address = "https://hml.apisulcard.com.br/webservice/roteirizadorservice.asmx";

    public java.lang.String getRoteirizadorServiceSoapAddress() {
        return RoteirizadorServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RoteirizadorServiceSoapWSDDServiceName = "RoteirizadorServiceSoap";

    public java.lang.String getRoteirizadorServiceSoapWSDDServiceName() {
        return RoteirizadorServiceSoapWSDDServiceName;
    }

    public void setRoteirizadorServiceSoapWSDDServiceName(java.lang.String name) {
        RoteirizadorServiceSoapWSDDServiceName = name;
    }

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RoteirizadorServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRoteirizadorServiceSoap(endpoint);
    }

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_BindingStub _stub = new com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_BindingStub(portAddress, this);
            _stub.setPortName(getRoteirizadorServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRoteirizadorServiceSoapEndpointAddress(java.lang.String address) {
        RoteirizadorServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap12Stub _stub = new com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap12Stub(new java.net.URL(RoteirizadorServiceSoap12_address), this);
                _stub.setPortName(getRoteirizadorServiceSoap12WSDDServiceName());
                return _stub;
            }
            if (com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_BindingStub _stub = new com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_BindingStub(new java.net.URL(RoteirizadorServiceSoap_address), this);
                _stub.setPortName(getRoteirizadorServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RoteirizadorServiceSoap12".equals(inputPortName)) {
            return getRoteirizadorServiceSoap12();
        }
        else if ("RoteirizadorServiceSoap".equals(inputPortName)) {
            return getRoteirizadorServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "RoteirizadorService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "RoteirizadorServiceSoap12"));
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "RoteirizadorServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RoteirizadorServiceSoap12".equals(portName)) {
            setRoteirizadorServiceSoap12EndpointAddress(address);
        }
        else 
if ("RoteirizadorServiceSoap".equals(portName)) {
            setRoteirizadorServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
