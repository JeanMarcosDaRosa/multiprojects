/**
 * DeclaracaoRota.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public class DeclaracaoRota  implements java.io.Serializable {
    private java.lang.String codigoTransportadora;

    private java.lang.String nomeRota;

    private java.lang.String descricaoRota;

    private java.lang.String cepOrigem;

    private java.lang.String UFEstadoOrigem;

    private java.lang.String nomeCidadeOrigem;

    private java.lang.String logradouroOrigem;

    private java.lang.String cepDestino;

    private java.lang.String UFEstadoDestino;

    private java.lang.String nomeCidadeDestino;

    private java.lang.String logradouroDestino;

    private com.tn3.reteirizador.axis.base.ParadaRotaDeclaracao[] paradas;

    private java.lang.Integer tipoRota;

    private java.lang.Integer tipoVeiculo;

    public DeclaracaoRota() {
    }

    public DeclaracaoRota(
           java.lang.String codigoTransportadora,
           java.lang.String nomeRota,
           java.lang.String descricaoRota,
           java.lang.String cepOrigem,
           java.lang.String UFEstadoOrigem,
           java.lang.String nomeCidadeOrigem,
           java.lang.String logradouroOrigem,
           java.lang.String cepDestino,
           java.lang.String UFEstadoDestino,
           java.lang.String nomeCidadeDestino,
           java.lang.String logradouroDestino,
           com.tn3.reteirizador.axis.base.ParadaRotaDeclaracao[] paradas,
           java.lang.Integer tipoRota,
           java.lang.Integer tipoVeiculo) {
           this.codigoTransportadora = codigoTransportadora;
           this.nomeRota = nomeRota;
           this.descricaoRota = descricaoRota;
           this.cepOrigem = cepOrigem;
           this.UFEstadoOrigem = UFEstadoOrigem;
           this.nomeCidadeOrigem = nomeCidadeOrigem;
           this.logradouroOrigem = logradouroOrigem;
           this.cepDestino = cepDestino;
           this.UFEstadoDestino = UFEstadoDestino;
           this.nomeCidadeDestino = nomeCidadeDestino;
           this.logradouroDestino = logradouroDestino;
           this.paradas = paradas;
           this.tipoRota = tipoRota;
           this.tipoVeiculo = tipoVeiculo;
    }


    /**
     * Gets the codigoTransportadora value for this DeclaracaoRota.
     * 
     * @return codigoTransportadora
     */
    public java.lang.String getCodigoTransportadora() {
        return codigoTransportadora;
    }


    /**
     * Sets the codigoTransportadora value for this DeclaracaoRota.
     * 
     * @param codigoTransportadora
     */
    public void setCodigoTransportadora(java.lang.String codigoTransportadora) {
        this.codigoTransportadora = codigoTransportadora;
    }


    /**
     * Gets the nomeRota value for this DeclaracaoRota.
     * 
     * @return nomeRota
     */
    public java.lang.String getNomeRota() {
        return nomeRota;
    }


    /**
     * Sets the nomeRota value for this DeclaracaoRota.
     * 
     * @param nomeRota
     */
    public void setNomeRota(java.lang.String nomeRota) {
        this.nomeRota = nomeRota;
    }


    /**
     * Gets the descricaoRota value for this DeclaracaoRota.
     * 
     * @return descricaoRota
     */
    public java.lang.String getDescricaoRota() {
        return descricaoRota;
    }


    /**
     * Sets the descricaoRota value for this DeclaracaoRota.
     * 
     * @param descricaoRota
     */
    public void setDescricaoRota(java.lang.String descricaoRota) {
        this.descricaoRota = descricaoRota;
    }


    /**
     * Gets the cepOrigem value for this DeclaracaoRota.
     * 
     * @return cepOrigem
     */
    public java.lang.String getCepOrigem() {
        return cepOrigem;
    }


    /**
     * Sets the cepOrigem value for this DeclaracaoRota.
     * 
     * @param cepOrigem
     */
    public void setCepOrigem(java.lang.String cepOrigem) {
        this.cepOrigem = cepOrigem;
    }


    /**
     * Gets the UFEstadoOrigem value for this DeclaracaoRota.
     * 
     * @return UFEstadoOrigem
     */
    public java.lang.String getUFEstadoOrigem() {
        return UFEstadoOrigem;
    }


    /**
     * Sets the UFEstadoOrigem value for this DeclaracaoRota.
     * 
     * @param UFEstadoOrigem
     */
    public void setUFEstadoOrigem(java.lang.String UFEstadoOrigem) {
        this.UFEstadoOrigem = UFEstadoOrigem;
    }


    /**
     * Gets the nomeCidadeOrigem value for this DeclaracaoRota.
     * 
     * @return nomeCidadeOrigem
     */
    public java.lang.String getNomeCidadeOrigem() {
        return nomeCidadeOrigem;
    }


    /**
     * Sets the nomeCidadeOrigem value for this DeclaracaoRota.
     * 
     * @param nomeCidadeOrigem
     */
    public void setNomeCidadeOrigem(java.lang.String nomeCidadeOrigem) {
        this.nomeCidadeOrigem = nomeCidadeOrigem;
    }


    /**
     * Gets the logradouroOrigem value for this DeclaracaoRota.
     * 
     * @return logradouroOrigem
     */
    public java.lang.String getLogradouroOrigem() {
        return logradouroOrigem;
    }


    /**
     * Sets the logradouroOrigem value for this DeclaracaoRota.
     * 
     * @param logradouroOrigem
     */
    public void setLogradouroOrigem(java.lang.String logradouroOrigem) {
        this.logradouroOrigem = logradouroOrigem;
    }


    /**
     * Gets the cepDestino value for this DeclaracaoRota.
     * 
     * @return cepDestino
     */
    public java.lang.String getCepDestino() {
        return cepDestino;
    }


    /**
     * Sets the cepDestino value for this DeclaracaoRota.
     * 
     * @param cepDestino
     */
    public void setCepDestino(java.lang.String cepDestino) {
        this.cepDestino = cepDestino;
    }


    /**
     * Gets the UFEstadoDestino value for this DeclaracaoRota.
     * 
     * @return UFEstadoDestino
     */
    public java.lang.String getUFEstadoDestino() {
        return UFEstadoDestino;
    }


    /**
     * Sets the UFEstadoDestino value for this DeclaracaoRota.
     * 
     * @param UFEstadoDestino
     */
    public void setUFEstadoDestino(java.lang.String UFEstadoDestino) {
        this.UFEstadoDestino = UFEstadoDestino;
    }


    /**
     * Gets the nomeCidadeDestino value for this DeclaracaoRota.
     * 
     * @return nomeCidadeDestino
     */
    public java.lang.String getNomeCidadeDestino() {
        return nomeCidadeDestino;
    }


    /**
     * Sets the nomeCidadeDestino value for this DeclaracaoRota.
     * 
     * @param nomeCidadeDestino
     */
    public void setNomeCidadeDestino(java.lang.String nomeCidadeDestino) {
        this.nomeCidadeDestino = nomeCidadeDestino;
    }


    /**
     * Gets the logradouroDestino value for this DeclaracaoRota.
     * 
     * @return logradouroDestino
     */
    public java.lang.String getLogradouroDestino() {
        return logradouroDestino;
    }


    /**
     * Sets the logradouroDestino value for this DeclaracaoRota.
     * 
     * @param logradouroDestino
     */
    public void setLogradouroDestino(java.lang.String logradouroDestino) {
        this.logradouroDestino = logradouroDestino;
    }


    /**
     * Gets the paradas value for this DeclaracaoRota.
     * 
     * @return paradas
     */
    public com.tn3.reteirizador.axis.base.ParadaRotaDeclaracao[] getParadas() {
        return paradas;
    }


    /**
     * Sets the paradas value for this DeclaracaoRota.
     * 
     * @param paradas
     */
    public void setParadas(com.tn3.reteirizador.axis.base.ParadaRotaDeclaracao[] paradas) {
        this.paradas = paradas;
    }


    /**
     * Gets the tipoRota value for this DeclaracaoRota.
     * 
     * @return tipoRota
     */
    public java.lang.Integer getTipoRota() {
        return tipoRota;
    }


    /**
     * Sets the tipoRota value for this DeclaracaoRota.
     * 
     * @param tipoRota
     */
    public void setTipoRota(java.lang.Integer tipoRota) {
        this.tipoRota = tipoRota;
    }


    /**
     * Gets the tipoVeiculo value for this DeclaracaoRota.
     * 
     * @return tipoVeiculo
     */
    public java.lang.Integer getTipoVeiculo() {
        return tipoVeiculo;
    }


    /**
     * Sets the tipoVeiculo value for this DeclaracaoRota.
     * 
     * @param tipoVeiculo
     */
    public void setTipoVeiculo(java.lang.Integer tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeclaracaoRota)) return false;
        DeclaracaoRota other = (DeclaracaoRota) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoTransportadora==null && other.getCodigoTransportadora()==null) || 
             (this.codigoTransportadora!=null &&
              this.codigoTransportadora.equals(other.getCodigoTransportadora()))) &&
            ((this.nomeRota==null && other.getNomeRota()==null) || 
             (this.nomeRota!=null &&
              this.nomeRota.equals(other.getNomeRota()))) &&
            ((this.descricaoRota==null && other.getDescricaoRota()==null) || 
             (this.descricaoRota!=null &&
              this.descricaoRota.equals(other.getDescricaoRota()))) &&
            ((this.cepOrigem==null && other.getCepOrigem()==null) || 
             (this.cepOrigem!=null &&
              this.cepOrigem.equals(other.getCepOrigem()))) &&
            ((this.UFEstadoOrigem==null && other.getUFEstadoOrigem()==null) || 
             (this.UFEstadoOrigem!=null &&
              this.UFEstadoOrigem.equals(other.getUFEstadoOrigem()))) &&
            ((this.nomeCidadeOrigem==null && other.getNomeCidadeOrigem()==null) || 
             (this.nomeCidadeOrigem!=null &&
              this.nomeCidadeOrigem.equals(other.getNomeCidadeOrigem()))) &&
            ((this.logradouroOrigem==null && other.getLogradouroOrigem()==null) || 
             (this.logradouroOrigem!=null &&
              this.logradouroOrigem.equals(other.getLogradouroOrigem()))) &&
            ((this.cepDestino==null && other.getCepDestino()==null) || 
             (this.cepDestino!=null &&
              this.cepDestino.equals(other.getCepDestino()))) &&
            ((this.UFEstadoDestino==null && other.getUFEstadoDestino()==null) || 
             (this.UFEstadoDestino!=null &&
              this.UFEstadoDestino.equals(other.getUFEstadoDestino()))) &&
            ((this.nomeCidadeDestino==null && other.getNomeCidadeDestino()==null) || 
             (this.nomeCidadeDestino!=null &&
              this.nomeCidadeDestino.equals(other.getNomeCidadeDestino()))) &&
            ((this.logradouroDestino==null && other.getLogradouroDestino()==null) || 
             (this.logradouroDestino!=null &&
              this.logradouroDestino.equals(other.getLogradouroDestino()))) &&
            ((this.paradas==null && other.getParadas()==null) || 
             (this.paradas!=null &&
              java.util.Arrays.equals(this.paradas, other.getParadas()))) &&
            ((this.tipoRota==null && other.getTipoRota()==null) || 
             (this.tipoRota!=null &&
              this.tipoRota.equals(other.getTipoRota()))) &&
            ((this.tipoVeiculo==null && other.getTipoVeiculo()==null) || 
             (this.tipoVeiculo!=null &&
              this.tipoVeiculo.equals(other.getTipoVeiculo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoTransportadora() != null) {
            _hashCode += getCodigoTransportadora().hashCode();
        }
        if (getNomeRota() != null) {
            _hashCode += getNomeRota().hashCode();
        }
        if (getDescricaoRota() != null) {
            _hashCode += getDescricaoRota().hashCode();
        }
        if (getCepOrigem() != null) {
            _hashCode += getCepOrigem().hashCode();
        }
        if (getUFEstadoOrigem() != null) {
            _hashCode += getUFEstadoOrigem().hashCode();
        }
        if (getNomeCidadeOrigem() != null) {
            _hashCode += getNomeCidadeOrigem().hashCode();
        }
        if (getLogradouroOrigem() != null) {
            _hashCode += getLogradouroOrigem().hashCode();
        }
        if (getCepDestino() != null) {
            _hashCode += getCepDestino().hashCode();
        }
        if (getUFEstadoDestino() != null) {
            _hashCode += getUFEstadoDestino().hashCode();
        }
        if (getNomeCidadeDestino() != null) {
            _hashCode += getNomeCidadeDestino().hashCode();
        }
        if (getLogradouroDestino() != null) {
            _hashCode += getLogradouroDestino().hashCode();
        }
        if (getParadas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParadas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParadas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipoRota() != null) {
            _hashCode += getTipoRota().hashCode();
        }
        if (getTipoVeiculo() != null) {
            _hashCode += getTipoVeiculo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeclaracaoRota.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "DeclaracaoRota"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTransportadora");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CodigoTransportadora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NomeRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DescricaoRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CepOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UFEstadoOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "UFEstadoOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeCidadeOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NomeCidadeOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logradouroOrigem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "LogradouroOrigem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CepDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UFEstadoDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "UFEstadoDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeCidadeDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NomeCidadeDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logradouroDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "LogradouroDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paradas");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Paradas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ParadaRotaDeclaracao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "ParadaRotaDeclaracao"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TipoRota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoVeiculo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TipoVeiculo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
