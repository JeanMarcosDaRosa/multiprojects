/**
 * RoteirizadorService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tn3.reteirizador.axis.base;

public interface RoteirizadorService extends javax.xml.rpc.Service {
    public java.lang.String getRoteirizadorServiceSoap12Address();

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getRoteirizadorServiceSoapAddress();

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.tn3.reteirizador.axis.base.RoteirizadorServiceSoap_PortType getRoteirizadorServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
