/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulasenhas;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.binary.Base64;

public class CryptoUtil {
    
    private static CryptoUtil _instance;
    private String secret;
    private String iv;
    private Cipher cipher;
    private SecretKeySpec keySpec;
    private IvParameterSpec ivSpec;

    public static CryptoUtil getInstance() {
        return _instance;
    }

    static {
        try {
            _instance = new CryptoUtil();
        } catch (Exception e) {
            System.out.println("Falha ao iniciar " + CryptoUtil.class.getName() + e.getMessage());
            
        }
    }

    private static String padString(String source) {
        char paddingChar = ' ';
        int size = 16;
        int padLength = size - source.length() % size;

        for (int i = 0; i < padLength; i++) {
            source += paddingChar;
        }

        return source;
    }

    public CryptoUtil() throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException {
        secret = new String("EkRtyg!@#$#%-906");
        iv = new String("asd098d74j(89!@d");
        cipher = Cipher.getInstance("AES/CBC/NoPadding");
        keySpec = new SecretKeySpec(secret.getBytes(), "AES");
        ivSpec = new IvParameterSpec(iv.getBytes());
    }

    public final String encrypt(final String text) {
        String ret = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            CipherOutputStream cos = new CipherOutputStream(os, cipher);
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(cos));
            pw.println(padString(text.trim()));
            pw.close();

            ret = new String(Base64.encodeBase64(os.toByteArray()));
        } catch (Exception e) {
            System.out.println(e);
        }
        return ret;
    }

    public final String decrypt(final String text) {
        String ret = null;
        try {
            String enc64 = text;
            byte[] enc64bytes = enc64.getBytes();
            byte[] dec64bytes = Base64.decodeBase64(enc64bytes);

            // set decrypt params
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            ByteArrayInputStream fis = new ByteArrayInputStream(dec64bytes);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            ByteArrayOutputStream fos = new ByteArrayOutputStream();
            // decrypting
            byte[] b = new byte[8];
            int i = 0;
            while ((i = cis.read(b)) != -1) {
                fos.write(b, 0, i);
            }
            fos.flush();
            fos.close();
            cis.close();
            fis.close();
            ret = fos.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
        return ret.trim();
    }
}
