 package com.sun.tools.doclets.formats.html.resources;
 
 import java.util.ListResourceBundle;
 
 public final class standard_pt_BR extends ListResourceBundle
 {
   protected final Object[][] getContents()
   {
     return new Object[][] {
                { "doclet.0_Fields_and_Methods", "&quot;{0}&quot; Campos e m&eacute;todos" },
                { "doclet.All_Packages", "Todos os pacotes" },
                { "doclet.Annotation_Type_Hierarchy", "Hierarquia de Anota&ccedil;&otilde;es de Tipo" },
                { "doclet.Blank", "Em Branco" },
                { "doclet.CLASSES", "CLASSES" },
                { "doclet.Cannot_handle_no_packages", "Cannot handle no packages." },
                { "doclet.ClassUse_Annotation", "Classes em {1} com anota&ccedil;&otilde;es do tipo {0}" },
                { "doclet.ClassUse_Classes.in.0.used.by.1", "Classes em {0} usadas por {1}" },
                { "doclet.ClassUse_ConstructorAnnotations", "Construtores em {1} com anota&ccedil;&otilde;es do tipo {0}" },
                { "doclet.ClassUse_ConstructorArgs", "Construtores em {1} com par&acirc;metros do tipo {0}" },
                { "doclet.ClassUse_ConstructorArgsTypeParameters", "Par&acirc;metros do contrutor em {1} com argumentos de tipo do tipo {0}" },
                { "doclet.ClassUse_ConstructorParameterAnnotations", "Constructor parameters in {1} com anota&ccedil;&otilde;es do tipo {0}" },
                { "doclet.ClassUse_ConstructorThrows", "Construtores em {1} que lan&ccedil;am {0}" },
                { "doclet.ClassUse_Field", "Atributos em {1} declarados como {0}" },
                { "doclet.ClassUse_FieldAnnotations", "Atributos em {1} com anota&ccedil;&otilde;es do tipo {0}" },
                { "doclet.ClassUse_FieldTypeParameter", "Atributos em {1} com par&acirc;metros de tipo do tipo {0}" },
                { "doclet.ClassUse_ImplementingClass", "Classes in {1} que implementa {0}" },
                { "doclet.ClassUse_MethodAnnotations", "M&eacute;todos em {1} com anota&ccedil;&otilde;es do tipo {0}" },
                { "doclet.ClassUse_MethodArgs", "M&eacute;todos em {1} com par&acirc;metros do tipo {0}" },
                { "doclet.ClassUse_MethodArgsTypeParameters", "Par&acirc;metros de m&eacute;todo em {1} com argumentos de tipo do tipo {0}" },
                { "doclet.ClassUse_MethodParameterAnnotations", "Par&acirc;metros de m&eacute;todos em {1} com anota&ccedil;&otilde;es do tipo {0}" },
                { "doclet.ClassUse_MethodReturn", "M&eacute;todos em {1} que retornam {0}" },
                { "doclet.ClassUse_MethodReturnTypeParameter", "M&eacute;todos em {1} que retornam tipos com argumentos do tipo {0}" },
                { "doclet.ClassUse_MethodThrows", "M&eacute;todos em {1} que lam&ccedil;am {0}" },
                { "doclet.ClassUse_MethodTypeParameter", "M&eacute;todos em {1} com par&acirc;metros de tipo do tipo {0}" },
                { "doclet.ClassUse_No.usage.of.0", "N&atilde;o usado de {0}" },
                { "doclet.ClassUse_PackageAnnotation", "Pacotes com anota&ccedil;&otilde;es de tipos {0}" },
                { "doclet.ClassUse_Packages.that.use.0", "Pacotes que usam {0}" },
                { "doclet.ClassUse_Subclass", "Subclasses de {0} em {1}" },
                { "doclet.ClassUse_Subinterface", "Subinterfaces de {0} em {1}" },
                { "doclet.ClassUse_Title", "Usos de {0}<br>{1}" },
                { "doclet.ClassUse_TypeParameter", "Classes em {1} com par&acirc;metros de tipo do tipo {0}" },
                { "doclet.ClassUse_Uses.of.0.in.1", "Usos de {0} em {1}" },
                { "doclet.Class_Hierarchy", "Hierarquia de Classes" },
                { "doclet.Constructor_for", "Construtor para {0}" },
                { "doclet.Contents", "Conte&uacute;do" },
                { "doclet.Deprecated_API", "API depreciada" },
                { "doclet.Deprecated_Annotation_Type_Members", "Anota&ccedil;&otilde;es de tipos de elementos depreciados" },
                { "doclet.Deprecated_Annotation_Types", "Anota&ccedil;&otilde;es de tipos depreciados" },
                { "doclet.Deprecated_Classes", "Classes depreciadas" },
                { "doclet.Deprecated_Constructors", "Construtores depreciados" },
                { "doclet.Deprecated_Enum_Constants", "Enumera&ccedil;&otilde;es Constantes depreciadas" },
                { "doclet.Deprecated_Enums", "Enumera&ccedil;&otilde;es depreciadas" },
                { "doclet.Deprecated_Errors", "Erros depreciados" },
                { "doclet.Deprecated_Exceptions", "Excess&otilde;es depreciadas" },
                { "doclet.Deprecated_Fields", "Atributos depreciados" },
                { "doclet.Deprecated_Interfaces", "Interfaces depreciadas" },
                { "doclet.Deprecated_List", "Listas depreciadas" },
                { "doclet.Deprecated_Methods", "M&eacute;todos depreciados" },
                { "doclet.Description", "Descri&ccedil;&atilde;o" },
                { "doclet.Description_From_Class", "Descri&ccedil;&atilde;o copiada da classe: {0}" },
                { "doclet.Description_From_Interface", "Descri&ccedil;&atilde;o copiada da interface: {0}" },
                { "doclet.Detail", "DETALHES:" },
                { "doclet.Docs_generated_by_Javadoc", "Documenta&ccedil;&atilde;o gerada pelo Javadoc (tradu&ccedil;&atilde;o parcial por Kal&eacute;u Caminha / Jean Marcos da Rosa)." },
                { "doclet.Enclosing_Class", "Classe delimitadora:" },
                { "doclet.Enclosing_Interface", "Interface delimitadora:" },
                { "doclet.Enum_Hierarchy", "Hierarquia de Enumera&ccedil;&atilde;o" },
                { "doclet.Error_in_packagelist", "Erro no uso da op&ccedil;&atilde;o -group: {0} {1}" },
                { "doclet.FRAMES", "FRAMES" },
                { "doclet.Factory_Method_Detail", "Static Factory Method Detail" },
                { "doclet.File_error", "Erro lendo arquivo: {0}" },
                { "doclet.Following_From_Class", "Following copied from class: {0}" },
                { "doclet.Following_From_Interface", "Following copied from interface: {0}" },
                { "doclet.Frame_Alert", "Frame de Alerta" },
                { "doclet.Frame_Output", "Frame de Sa&iacute;da" },
                { "doclet.Frame_Version", "Frame de Vers&atilde;o" },
                { "doclet.Frame_Warning_Message", "Este documento destina-se a ser visto usando o recurso de frames. Se voc&ecirc; ver esta mensagem, voc&ecirc; est&aacute; usando um cliente web sem capacidade para frames." },
                { "doclet.Frames", "Frames" },
                { "doclet.Generated_Docs_Untitled", "Documenta&ccedil;&atilde;o Gerada (Sem t&iacute;tulo)" },
                { "doclet.Groupname_already_used", "Na op&ccedil;&atilde;o -group, groupname j&aacute; esta sendo usado: {0}" },
                { "doclet.Help", "Ajuda" },
                { "doclet.Help_annotation_type_line_1", "Cada tipo de anota&ccedil;&atilde;o tem sua pr&oacute;pria p&aacute;gina separada com as seguintes se&ccedil;&otilde;es:" },
                { "doclet.Help_annotation_type_line_2", "Annotation Type declaration" },
                { "doclet.Help_annotation_type_line_3", "Annotation Type description" },
                { "doclet.Help_enum_line_1", "Cada enumera&ccedil;&atilde;o tem sua pr&oacute;pria p&aacute;gina separada com as seguintes se&ccedil;&otilde;es:" },
                { "doclet.Help_enum_line_2", "Declara&ccedil;&atilde;o da enumera&ccedil;&atilde;o" },
                { "doclet.Help_enum_line_3", "Descri&ccedil;&atilde;o da enumera&ccedil;&atilde;o" },
                { "doclet.Help_line_1", "Como este documento da API est&aacute; organizado" },
                { "doclet.Help_line_10", "Todas as classes implementadas conhecidas" },
                { "doclet.Help_line_11", "Declara&ccedil;&atilde;o de classe/interface" },
                { "doclet.Help_line_12", "Descri&ccedil;&atilde;o de classe/interface" },
                { "doclet.Help_line_13", "Cada entrada resumo cont&eacute;m a primeira frase da descri&ccedil;&atilde;o detalhada para esse item. As entradas s&atilde;o ordenadas alfab&eacute;ticamente, enquanto as descri&ccedil;&otilde;es detalhadas est&atilde;o na ordem em que aparecem no c&oacute;digo-fonte. Isso preserva os agrupamentos l&oacute;gicos definidos pelo programador." },
                { "doclet.Help_line_14", "Uso" },
                { "doclet.Help_line_15", "Each documented package, class and interface has its own Use page.  This page describes what packages, classes, methods, constructors and fields use any part of the given class or package. Given a class or interface A, its Use page includes subclasses of A, fields declared as A, methods that return A, and methods and constructors with parameters of type A.  You can access this page by first going to the package, class or interface, then clicking on the \"Use\" link in the navigation bar." },
                { "doclet.Help_line_16", "Hierarquia de Classes" },
                { "doclet.Help_line_17_with_tree_link", "There is a {0} page for all packages, plus a hierarchy for each package. Each hierarchy page contains a list of classes and a list of interfaces. The classes are organized by inheritance structure starting with <code>java.lang.Object</code>. The interfaces do not inherit from <code>java.lang.Object</code>." },
                { "doclet.Help_line_18", "When viewing the Overview page, clicking on \"Tree\" displays the hierarchy for all packages." },
                { "doclet.Help_line_19", "When viewing a particular package, class or interface page, clicking \"Tree\" displays the hierarchy for only that package." },
                { "doclet.Help_line_2", "This API (Application Programming Interface) document has pages corresponding to the items in the navigation bar, described as follows." },
                { "doclet.Help_line_20_with_deprecated_api_link", "The {0} page lists all of the API that have been deprecated. A deprecated API is not recommended for use, generally due to improvements, and a replacement API is usually given. Deprecated APIs may be removed in future implementations." },
                { "doclet.Help_line_21", "Principal" },
                { "doclet.Help_line_22", "O {0} cont&eacute;m uma lista alfab&eacute;tica de todas as classes, interfaces, construtores, m&eacute;todos, e atributos." },
                { "doclet.Help_line_23", "Anterior/Pr&oacute;ximo" },
                { "doclet.Help_line_24", "These links take you to the next or previous class, interface, package, or related page.<br/>" },
                { "doclet.Help_line_25", "Frames/Sem Frames" },
                { "doclet.Help_line_26", "These links show and hide the HTML frames.  All pages are available with or without frames." },
                { "doclet.Help_line_27", "Each serializable or externalizable class has a description of its serialization fields and methods. This information is of interest to re-implementors, not to developers using the API. While there is no link in the navigation bar, you can get to this information by going to any serialized class and clicking \"Serialized Form\" in the \"See also\" section of the class description." },
                { "doclet.Help_line_28", "The <a href=\"constant-values.html\">Constant Field Values</a> page lists the static final fields and their values." },
                { "doclet.Help_line_29", "This help file applies to API documentation generated using the standard doclet." },
                { "doclet.Help_line_3", "A p&aacute;gina {0} &eacute; a p&aacute;gina inicial dos documentos desta API e prov&ecirc; uma lista de todos os pacotes e um sum&aacute;rio para cada.  Esta p&aacute;gina tamb&eacute;m pode conter uma descri&ccedil;&atilde;o geral do conjunto de pacotes." },
                { "doclet.Help_line_4", "Each package has a page that contains a list of its classes and interfaces, with a summary for each. This page can contain four categories:" },
                { "doclet.Help_line_5", "Classe/Interface" },
                { "doclet.Help_line_6", "Each class, interface, nested class and nested interface has its own separate page. Each of these pages has three sections consisting of a class/interface description, summary tables, and detailed member descriptions:" },
                { "doclet.Help_line_7", "Diagrama de heran&ccedil;a de classe" },
                { "doclet.Help_line_8", "Subclasses diretas" },
                { "doclet.Help_line_9", "Todas as subinterfaces conhecidas" },
                { "doclet.Help_title", "Ajuda API" },
                { "doclet.Hide_Lists", "ESCONDER LISTAS" },
                { "doclet.Hierarchy_For_All_Packages", "Hierarquia para todos os pacotes" },
                { "doclet.Hierarchy_For_Package", "Hierarquia para o pacote {0}" },
                { "doclet.Href_Annotation_Title", "Anota&ccedil;&atilde;o em {0}" },
                { "doclet.Href_Class_Or_Interface_Title", "classe ou interface em {0}" },
                { "doclet.Href_Class_Title", "classe em {0}" },
                { "doclet.Href_Enum_Title", "enumera&ccedil;&atilde;o em {0}" },
                { "doclet.Href_Interface_Title", "interface em {0}" },
                { "doclet.Href_Type_Param_Title", "Tipo de par&acirc;metro em {0}" },
                { "doclet.Implementing_Classes", "Todas as classes implementadas conhecidas:" },
                { "doclet.Index", "&Iacute;ndice" },
                { "doclet.Index_of_Fields_and_Methods", "&Iacute;ndice dos campos e m&eacute;todos" },
                { "doclet.Inherited_API_Summary", "Resumo da API herdada" },
                { "doclet.Interface_Hierarchy", "Hierarquia de Interface" },
                { "doclet.Interfaces_Italic", "Interfaces (italic)" },
                { "doclet.Link_To", "Link para" },
                { "doclet.MEMBERS", "INTEGRANTES" },
                { "doclet.MalformedURL", "URL Deformada: {0}" },
                { "doclet.Method_in", "M&eacute;todo em {0}" },
                { "doclet.NONE", "Vazio" },
                { "doclet.NO_FRAMES", "SEM FRAMES" },
                { "doclet.Next", "Pr&oacute;ximo" },
                { "doclet.Next_Class", "Pr&oacute;xima Classe" },
                { "doclet.Next_Letter", "Pr&oacute;xima Letra" },
                { "doclet.Next_Package", "Pr&oacute;ximo Pacote" },
                { "doclet.No_Non_Deprecated_Classes_To_Document", "No non-deprecated classes found to document." },
                { "doclet.No_Package_Comment_File", "For Package {0} Package.Comment file not found" },
                { "doclet.No_Source_For_Class", "Source information for class {0} not available." },
                { "doclet.Non_Frame_Version", "Vers&atilde;o sem frames." },
                { "doclet.None", "Vazio" },
                { "doclet.Note_0_is_deprecated", "Nota: {0} est&aacute; depreciado." },
                { "doclet.Option", "Op&ccedil;&atilde;o" },
                { "doclet.Or", "Ou" },
                { "doclet.Other_Packages", "Outros pacotes" },
                { "doclet.Overrides", "M&eacute;todos sobrescritos:" },
                { "doclet.Overview", "Vis&atilde;o Geral" },
                { "doclet.Overview-Member-Frame", "Overview Member Frame" },
                { "doclet.Package", "Pacote" },
                { "doclet.Package_Description", "Pacote {0} Descri&ccedil;&atilde;o" },
                { "doclet.Package_Hierarchies", "Hierarquias de pacote:" },
                { "doclet.Prev", "ANTERIOR" },
                { "doclet.Prev_Class", "Classe anterior" },
                { "doclet.Prev_Letter", "Letra anterior" },
                { "doclet.Prev_Package", "Pacote anterior" },
                { "doclet.Same_package_name_used", "Formato do nome do pacote usado duas vezes: {0}" },
                { "doclet.Serialization.Excluded_Class", "Non-transient field {1} uses excluded class {0}." },
                { "doclet.Serialization.Nonexcluded_Class", "Non-transient field {1} uses hidden, non-included class {0}." },
                { "doclet.Show_Lists", "Exibir Listas" },
                { "doclet.Skip_navigation_links", "Pular links de navega&ccedil;&atilde;o" },
                { "doclet.Source_Code", "C&oacute;digo fonte:" },
                { "doclet.Specified_By", "Especificado por:" },
                { "doclet.Standard_doclet_invoked", "Standard doclet invoked..." },
                { "doclet.Static_method_in", "M&eacute;todo est&aacute;tico em {0}" },
                { "doclet.Static_variable_in", "Vari&aacute;vel est&aacute;tica em {0}" },
                { "doclet.Style_Headings", "Cabe&ccedil;alhos" },
                { "doclet.Style_line_1", "Folha de estilo Javadoc" },
                { "doclet.Style_line_10", "Cores e fontes da barra de navega&ccedil;&atilde;o" },
                { "doclet.Style_line_11", "Dark Blue" },
                { "doclet.Style_line_2", "Define cores, fontes e outros atributos de estilo aqui para substituir os padr&otilde;es" },
                { "doclet.Style_line_3", "Cor de fundo da p&aacute;gina" },
                { "doclet.Style_line_4", "Tabela de cores" },
                { "doclet.Style_line_5", "Dark mauve" },
                { "doclet.Style_line_6", "Light mauve" },
                { "doclet.Style_line_7", "White" },
                { "doclet.Style_line_8", "Font used in left-hand frame lists" },
                { "doclet.Style_line_9", "Example of smaller, sans-serif font in frames" },
                { "doclet.Subclasses", "Subclasses diretas conhecidas:" },
                { "doclet.Subinterfaces", "Todas as Subinterfaces conhecidas:" },
                { "doclet.Summary", "RESUMO:" },
                { "doclet.The", "O(A)" },
                { "doclet.Tree", "Hierarquia" },
                { "doclet.URL_error", "Error fetching URL: {0}" },
                { "doclet.Variable_in", "Vari&aacute;vel em {0}" },
                { "doclet.Window_ClassUse_Header", "Usos de {0} {1}" },
                { "doclet.Window_Class_Hierarchy", "Hierarquia de classe" },
                { "doclet.Window_Deprecated_List", "Lista de Depreciados" },
                { "doclet.Window_Help_title", "Ajuda API" },
                { "doclet.Window_Overview", "Listagem da vis&atilde;o geral" },
                { "doclet.Window_Overview_Summary", "Vis&atilde;o Geral" },
                { "doclet.Window_Single_Index", "&Iacute;ndice" },
                { "doclet.Window_Split_Index", "{0}-&Iacute;ndice" },
                { "doclet.also", "igualmente" },
                { "doclet.build_version", "Standard Doclet vers&atilde;o {0}" },
                { "doclet.in_class", "{0} na classe {1}" },
                { "doclet.in_interface", "{0} na interface {1}" },
                { "doclet.link_option_twice", "Extern URL link option (link or linkoffline) used twice." },
                { "doclet.navAnnotationTypeMember", "ELEMENTO" },
                { "doclet.navAnnotationTypeOptionalMember", "OPCIONAL" },
                { "doclet.navAnnotationTypeRequiredMember", "OBRIGAT&Oacute;RIO" },
                { "doclet.navClassUse", "Uso" },
                { "doclet.navConstructor", "CONSTR" },
                { "doclet.navDeprecated", "Depreciados" },
                { "doclet.navEnum", "ENUM CONSTANTS" },
                { "doclet.navFactoryMethod", "FACTORY" },
                { "doclet.navField", "Campos" },
                { "doclet.navMethod", "M&eacute;todos" },
                { "doclet.navNested", "NESTED" },
                { "doclet.package", "pacote" },
                { "doclet.see.class_or_package_not_found", "Tag {0}: refer&ecirc;ncia n&atilde;o encontrada: {1}" },
                { "doclet.see.malformed_tag", "Tag {0}: Deformado: {1}" },
                { "doclet.throws", "lan&ccedil;a" },
                { "doclet.usage", "Provided by Standard doclet:\n-d <directory>                    Destination directory for output files\n-use                              Create class and package usage pages\n-version                          Include @version paragraphs\n-author                           Include @author paragraphs\n-docfilessubdirs                  Recursively copy doc-file subdirectories\n-splitindex                       Split index into one file per letter\n-windowtitle <text>               Browser window title for the documenation\n-doctitle <html-code>             Include title for the overview page\n-header <html-code>               Include header text for each page\n-footer <html-code>               Include footer text for each page\n-top    <html-code>               Include top text for each page\n-bottom <html-code>               Include bottom text for each page\n-link <url>                       Create links to javadoc output at <url>\n-linkoffline <url> <url2>         Link to docs at <url> using package list at <url2>\n-excludedocfilessubdir <name1>:.. Exclude any doc-files subdirectories with given name.\n-group <name> <p1>:<p2>..         Group specified packages together in overview page\n-nocomment                        Supress description and tags, generate only declarations.\n-nodeprecated                     Do not include @deprecated information\n-noqualifier <name1>:<name2>:...  Exclude the list of qualifiers from the output.\n-nosince                          Do not include @since information\n-notimestamp                      Do not include hidden time stamp\n-nodeprecatedlist                 Do not generate deprecated list\n-notree                           Do not generate class hierarchy\n-noindex                          Do not generate index\n-nohelp                           Do not generate help link\n-nonavbar                         Do not generate navigation bar\n-serialwarn                       Generate warning about @serial tag\n-tag <name>:<locations>:<header>  Specify single argument custom tags\n-taglet                           The fully qualified name of Taglet to register\n-tagletpath                       The path to Taglets\n-charset <charset>                Charset for cross-platform viewing of generated documentation.\n-helpfile <file>                  Include file that help link links to\n-linksource                       Generate source in HTML\n-sourcetab <tab length>           Specify the number of spaces each tab takes up in the source\n-keywords                         Include HTML meta tags with package, class and member info\n-stylesheetfile <path>            File to change style of the generated documentation\n-docencoding <name>               Output encoding name" } };
   }
 }